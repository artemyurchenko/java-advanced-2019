package ru.ifmo.rain.yurchenko.implementor;

import org.jetbrains.annotations.NotNull;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;

public class UnmodifiableReverseList<E> extends AbstractList<E> implements List<E> {
    private final @NotNull List<E> delegate;

    UnmodifiableReverseList(@NotNull final List<E> delegate) {
        if (delegate instanceof UnmodifiableReverseList) {
            var reversedOnce = (UnmodifiableReverseList<E>) delegate;
            if (reversedOnce.delegate instanceof UnmodifiableReverseList) {
                var reversedTwice = (UnmodifiableReverseList<E>) reversedOnce.delegate;
                this.delegate = reversedTwice.delegate;
                return;
            }
        }
        this.delegate = delegate;
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(int index, @NotNull Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public E get(int i) {
        return delegate.get(inv(i));
    }

    private int inv(int index) {
        return size() - 1 - index;
    }
}
