package ru.ifmo.rain.yurchenko.implementor;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class ArraySet<E> extends AbstractSet<E> implements NavigableSet<E> {
    private final @Nullable Comparator<? super E> cmp;
    private final @NotNull List<E> data;

    public ArraySet() {
        cmp = null;
        data = Collections.emptyList();
    }

    public ArraySet(final @NotNull Collection<? extends E> collection) {
        this(collection, null);
    }

    public ArraySet(final @NotNull Collection<? extends E> collection, @Nullable Comparator<? super E> cmp) {
        this.cmp = cmp;
        Set<E> set = new TreeSet<>(cmp);
        set.addAll(collection);
        data = new ArrayList<>(set);
    }

    private ArraySet(@Nullable final Comparator<? super E> cmp, @NotNull final List<E> data) {
        this.cmp = cmp;
        this.data = data;
    }

    @Nullable
    @Override
    public E lower(@NotNull final E e) {
        return getOrNull(lowerBound(Objects.requireNonNull(e)) - 1);
    }

    @Nullable
    @Override
    public E floor(@NotNull final E e) {
        return getOrNull(upperBound(Objects.requireNonNull(e)) - 1);
    }

    @Nullable
    @Override
    public E ceiling(@NotNull final E e) {
        return getOrNull(lowerBound(Objects.requireNonNull(e)));
    }

    @Nullable
    @Override
    public E higher(@NotNull final E e) {
        return getOrNull(upperBound(Objects.requireNonNull(e)));
    }

    @Nullable
    @Override
    public E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public E pollLast() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return data.size();
    }

    @SuppressWarnings("unchecked")
    @Override
    public boolean contains(final Object o) {
        if (o == null) {
            return false;
        }
        return Collections.binarySearch(data, (E) o, cmp) >= 0;
    }

    @NotNull
    @Override
    public Iterator<E> iterator() {
        return Collections.unmodifiableList(data).iterator();
    }

    @Override
    public boolean remove(final Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean addAll(@NotNull final Collection<? extends E> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(@NotNull final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(@NotNull final Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public NavigableSet<E> descendingSet() {
        Comparator<? super E> reversedCmp = cmp != null ? cmp.reversed() : Collections.reverseOrder();
        return new ArraySet<>(reversedCmp, new UnmodifiableReverseList<>(data));
    }

    @NotNull
    @Override
    public Iterator<E> descendingIterator() {
        return descendingSet().iterator();
    }

    @NotNull
    @Override
    public NavigableSet<E> subSet(final E fromElement, boolean fromInclusive, final E toElement, boolean toInclusive) {
        if (compare(fromElement, toElement) > 0) {
            throw new IllegalArgumentException();
        }
        return tailSet(fromElement, fromInclusive).headSet(toElement, toInclusive);
    }

    @NotNull
    @Override
    public NavigableSet<E> headSet(final E toElement, boolean inclusive) {
        var end = inclusive ? upperBound(toElement) : lowerBound(toElement);
        return new ArraySet<>(cmp, data.subList(0, end));
    }

    @NotNull
    @Override
    public NavigableSet<E> tailSet(final E fromElement, boolean inclusive) {
        var begin = inclusive ? lowerBound(fromElement) : upperBound(fromElement);
        return new ArraySet<>(cmp, data.subList(begin, size()));
    }

    @Nullable
    @Override
    public Comparator<? super E> comparator() {
        return cmp;
    }

    @NotNull
    @Override
    public SortedSet<E> subSet(final E fromElement, final E toElement) {
        return subSet(fromElement, true, toElement, false);
    }

    @NotNull
    @Override
    public SortedSet<E> headSet(final E toElement) {
        return headSet(toElement, false);
    }

    @NotNull
    @Override
    public SortedSet<E> tailSet(final E fromElement) {
        return tailSet(fromElement, true);
    }

    @Override
    public E first() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(0);
    }

    @Override
    public E last() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        return data.get(size() - 1);
    }

    @SuppressWarnings("unchecked")
    private int compare(@NotNull final E a, @NotNull final E b) {
        return cmp == null ? ((Comparable<E>) a).compareTo(b) : cmp.compare(a, b);
    }

    private @Nullable E getOrNull(int index) {
        if (index < 0 || index >= size()) {
            return null;
        }
        return data.get(index);
    }

    private int lowerBound(@NotNull final E e) {
        int index = Collections.binarySearch(data, e, cmp);
        if (index < 0) {
            index = ~index;
        }
        return index;
    }

    private int upperBound(@NotNull final E e) {
        var index = Collections.binarySearch(data, e, cmp);
        if (index < 0) {
            index = ~index;
        } else {
            ++index;
        }
        return index;
    }
}
