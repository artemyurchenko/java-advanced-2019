package ru.ifmo.rain.yurchenko.crawler;

import info.kgeorgiy.java.advanced.crawler.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

public class WebCrawler implements Crawler {
    private static final String USAGE_MESSAGE = "Usage: WebCrawler url [depth [downloads [extractors [perHost]]]]";

    private final Downloader downloader;
    private final ExecutorService downloadPool;
    private final ExecutorService extractPool;
    private final ConcurrentMap<String, Limiter> limiters;
    private final int perHost;

    public static void main(String[] args) {
        if (args.length < 1 || args.length > 5) {
            System.err.println(USAGE_MESSAGE);
            return;
        }
        String url = args[0];
        int depth, downloaders, extractors, perHost;
        try {
            int arg = 1;
            depth = tryExtractIntArg(args, arg++, 1);
            downloaders = tryExtractIntArg(args, arg++, Integer.MAX_VALUE);
            extractors = tryExtractIntArg(args, arg++, Integer.MAX_VALUE);
            perHost = tryExtractIntArg(args, arg++, Integer.MAX_VALUE);
        } catch (NumberFormatException ignored) {
            System.err.printf("Invalid parameter, number expected%n%s%n", USAGE_MESSAGE);
            return;
        }
        try {
            var path = Paths.get("crawler-downloaded");
            new WebCrawler(new CachingDownloader(path), downloaders, extractors, perHost).download(url, depth);
        } catch (IOException ignored) {
            System.err.println("Failed to store documents");
        }
    }

    public WebCrawler(final Downloader downloader, int downloaders, int extractors, int perHost) {
        this.downloader = downloader;
        this.downloadPool = Executors.newFixedThreadPool(downloaders);
        this.extractPool = Executors.newFixedThreadPool(extractors);
        this.limiters = new ConcurrentHashMap<>();
        this.perHost = perHost;
    }

    @Override
    public Result download(String startUrl, int depth) {
        var seen = ConcurrentHashMap.<String>newKeySet();
        var errors = new ConcurrentHashMap<String, IOException>();
        var phaser = new Phaser(1);
        var urlList = Collections.singletonList(startUrl);
        for (var i = 0; i < depth; ++i) {
            var nextStageUrls = Collections.synchronizedList(new ArrayList<String>());
            urlList.stream().filter(seen::add).forEach(url -> {
                try {
                    Limiter limiter = limiters.computeIfAbsent(URLUtils.getHost(url), s -> new Limiter(perHost));
                    phaser.register();
                    limiter.register(() -> processUrl(url, errors, phaser, nextStageUrls));
                } catch (IOException e) {
                    errors.put(url, e);
                }
            });
            phaser.arriveAndAwaitAdvance();
            urlList = nextStageUrls;
        }
        var downloaded = new ArrayList<>(seen);
        downloaded.removeAll(errors.keySet());
        return new Result(downloaded, errors);
    }

    @Override
    public void close() {
        downloadPool.shutdownNow();
        extractPool.shutdownNow();
    }

    private void processUrl(final String url,
                            final ConcurrentMap<String, IOException> errors,
                            final Phaser phaser,
                            final List<String> nextStageUrls) {
        try {
            Document page;
            try {
                page = downloader.download(url);
            } catch (IOException e) {
                errors.put(url, e);
                return;
            }
            phaser.register();
            extractPool.submit(() -> {
                try {
                    nextStageUrls.addAll(page.extractLinks());
                } catch (IOException e) {
                    errors.put(url, e);
                } finally {
                    phaser.arriveAndDeregister();
                }
            });
        } finally {
            phaser.arriveAndDeregister();
        }
    }

    private static int tryExtractIntArg(final String[] args, int index, int defaultValue) throws NumberFormatException {
        return args.length > index ? Integer.parseInt(args[index]) : defaultValue;
    }

    private class Limiter {
        private final int limit;
        private final AtomicInteger running = new AtomicInteger(0);
        private final Queue<Runnable> queue = new ConcurrentLinkedQueue<>();

        Limiter(int limit) {
            this.limit = limit;
        }

        void register(Runnable task) {
            if (running.getAndUpdate(x -> Math.min(limit, x + 1)) < limit) {
                launch(task);
            } else {
                queue.offer(task);
            }
        }

        private void launch(Runnable task) {
            downloadPool.submit(() -> {
                try {
                    task.run();
                } finally {
                    arrive();
                }
            });
        }

        private void arrive() {
            var nextTask = queue.poll();
            if (nextTask != null) {
                launch(nextTask);
            } else {
                running.decrementAndGet();
            }
        }
    }
}
