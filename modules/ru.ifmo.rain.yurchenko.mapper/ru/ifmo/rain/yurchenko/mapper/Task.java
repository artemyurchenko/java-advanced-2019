package ru.ifmo.rain.yurchenko.mapper;

public class Task<T> implements Runnable {
    private final Callable<T> callable;
    private volatile Object outcome = null;
    private volatile State state;

    Task(final Callable<T> callable) {
        this.callable = callable;
        state = State.NEW;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void run() {
        if (state == State.NEW) {
            state = State.RUNNING;
        } else {
            return;
        }
        try {
            outcome = callable.call();
            state = State.COMPLETED;
        } catch (Exception e) {
            outcome = e;
            state = State.EXCEPTIONAL;
        } finally {
            notify();
        }
    }

    /**
     * Waits until task is completed and get the outcome. If task completes unexceptionally returns the result.
     * If task throws exception, rethrows it wrapped in {@code ExecutionException}.
     *
     * @return result of computation, if no exceptions are thrown
     * @throws InterruptedException if interrupted
     * @throws RuntimeException     if computation throws
     */
    @SuppressWarnings("unchecked")
    public synchronized T get() throws InterruptedException, ExecutionException {
        while (state != State.COMPLETED && state != State.EXCEPTIONAL) {
            wait();
        }
        if (state == State.COMPLETED) {
            return (T) outcome;
        } else if (state == State.EXCEPTIONAL) {
            throw new ExecutionException((Exception) outcome);
        } else {
            throw new AssertionError("Unexpected state: " + state);
        }
    }

    enum State {NEW, RUNNING, COMPLETED, EXCEPTIONAL}
}

