package ru.ifmo.rain.yurchenko.mapper;

/**
 * Exception thrown when attempting to retrieve the result of a task
 * that aborted by throwing an exception.
 */
public class ExecutionException extends Exception {
    ExecutionException(Throwable cause) {
        super(cause);
    }
}
