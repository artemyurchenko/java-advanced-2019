package ru.ifmo.rain.yurchenko.mapper;

import info.kgeorgiy.java.advanced.mapper.ParallelMapper;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * {@code ParallelMapper} implementation
 * {@inheritDoc}
 */
public class ParallelMapperImpl implements ParallelMapper {
    private final RunnableQueue queue;
    private final List<Thread> threads;

    /**
     * Construct {@code ParallelMapperImpl} with fixed number of workers
     *
     * @param threadsCount number of threads to perform work
     */
    public ParallelMapperImpl(int threadsCount) {
        threads = new ArrayList<>(threadsCount);
        queue = new RunnableQueue();
        var worker = new Worker();
        for (int i = 0; i < threadsCount; ++i) {
            var thread = new Thread(worker);
            threads.add(thread);
            thread.start();
        }
    }

    private <T> Task<T> submit(Callable<T> callable) {
        var task = new Task<>(callable);
        queue.add(task);
        return task;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T, R> List<R> map(Function<? super T, ? extends R> function, List<? extends T> list) throws InterruptedException {
        var tasks = list.stream()
                .<Callable<R>>map(t -> () -> function.apply(t))
                .map(this::submit)
                .collect(Collectors.toList());
        List<R> mapped = new ArrayList<>(list.size());
        for (var task : tasks) {
            try {
                mapped.add(task.get());
            } catch (ExecutionException e) {
                var cause = e.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException) cause;
                } else if (cause instanceof Error) {
                    throw (Error) cause;
                }
                throw new AssertionError("Unexpected throwable", cause);
            }
        }
        return mapped;
    }

    @Override
    public void close() {
        threads.forEach(Thread::interrupt);
        for (var thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException ignored) {
            }
        }
    }

    private class Worker implements Runnable {
        @Override
        public void run() {
            try {
                while (!Thread.interrupted()) {
                    queue.take().run();
                }
            } catch (InterruptedException ignored) {
            } finally {
                Thread.currentThread().interrupt();
            }
        }
    }

    private class RunnableQueue {
        private final Queue<Runnable> queue = new ArrayDeque<>();

        synchronized Runnable take() throws InterruptedException {
            while (queue.isEmpty()) {
                wait();
            }
            return queue.poll();
        }

        synchronized void add(final Runnable runnable) {
            queue.add(runnable);
            notify();
        }
    }
}
