package ru.ifmo.rain.yurchenko.mapper;

@FunctionalInterface
public interface Callable<T> {
    T call() throws Exception;
}
