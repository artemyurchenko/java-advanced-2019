package ru.ifmo.rain.yurchenko.hello;

import info.kgeorgiy.java.advanced.hello.HelloClient;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;

public class HelloUDPClient implements HelloClient {
    private static final String USAGE = "HelloUDPClient host port prefix threads requests";
    private static final int SELECT_TIMEOUT = 100;

    public static void main(String[] args) {
        if (args.length != 5) {
            System.err.println("USAGE: " + USAGE);
            return;
        }
        try {
            int arg = 0;
            String host = args[arg++];
            int port = Integer.parseInt(args[arg++]);
            String prefix = args[arg++];
            int threads = Integer.parseInt(args[arg++]);
            int requests = Integer.parseInt(args[arg++]);
            new HelloUDPClient().run(host, port, prefix, threads, requests);
        } catch (NumberFormatException e) {
            System.err.printf("Invalid parameter, number expected%n%s%n", USAGE);
        }
    }

    @Override
    public void run(String host, int port, String prefix, int threads, int requests) {
        if (requests < 0) {
            throw new IllegalArgumentException("Number of requests can't be negative");
        }
        if (requests == 0) {
            return;
        }
        try {
            var hostAddress = new InetSocketAddress(host, port);
            var selector = Selector.open();
            for (int threadNo = 0; threadNo < threads; ++threadNo) {
                var channel = DatagramChannel.open();
                channel.connect(hostAddress);
                channel.configureBlocking(false);
                int bufferCapacity = channel.socket().getReceiveBufferSize();
                channel.register(selector, SelectionKey.OP_READ, new QueryInfo(threadNo, 0, bufferCapacity));
            }
            while (!selector.keys().isEmpty()) {
                var selectedCount = selector.select(key -> processRead(prefix, requests, key), SELECT_TIMEOUT);
                if (selectedCount == 0) {
                    for (var key : selector.keys()) {
                        sendQuery(key, prefix);
                    }
                }
            }
        } catch (IOException | UncheckedIOException e) {
            e.printStackTrace();
        }
    }

    private void processRead(String prefix, int requests, SelectionKey key) {
        try {
            var info = (QueryInfo) key.attachment();
            var receiveInfo = Utils.receiveQuery((DatagramChannel) key.channel(), info.buffer);
            String reply = receiveInfo.data;
            String query = makeQuery(prefix, info.threadNo, info.stageNo);
            boolean ok = areMatching(query, reply);
            System.out.printf("%s%nquery: %s%nreply: %s%n", ok ? "OK" : "ERR", query, reply);
            if (ok) {
                ++info.stageNo;
            }
            if (info.stageNo == requests) {
                key.cancel();
                key.channel().close();
            } else {
                sendQuery(key, prefix);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private void sendQuery(SelectionKey key, String prefix) throws IOException {
        var info = (QueryInfo) key.attachment();
        var query = makeQuery(prefix, info.threadNo, info.stageNo);
        var channel = (DatagramChannel) key.channel();
        channel.write(Utils.encodeBuffer(query));
    }

    private static boolean areMatching(String query, String reply) {
        return reply.contains(query);
    }

    private static String makeQuery(String prefix, int threadNo, int stageNo) {
        return prefix + threadNo + "_" + stageNo;
    }

    private static class QueryInfo {
        int threadNo;
        int stageNo;
        ByteBuffer buffer;

        QueryInfo(int threadNo, int stageNo, int bufferCapacity) {
            this.threadNo = threadNo;
            this.stageNo = stageNo;
            this.buffer = ByteBuffer.allocateDirect(bufferCapacity);
        }
    }
}

