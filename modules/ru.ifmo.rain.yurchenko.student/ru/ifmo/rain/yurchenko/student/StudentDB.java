package ru.ifmo.rain.yurchenko.student;

import info.kgeorgiy.java.advanced.student.AdvancedStudentGroupQuery;
import info.kgeorgiy.java.advanced.student.Group;
import info.kgeorgiy.java.advanced.student.Student;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StudentDB implements AdvancedStudentGroupQuery {
    private static final Comparator<Student> COMPARING_NAME = Comparator
            .comparing(Student::getLastName)
            .thenComparing(Student::getFirstName)
            .thenComparingInt(Student::getId);

    @Override
    public List<String> getFirstNames(final List<Student> students) {
        return extractField(students, Student::getFirstName);
    }

    @Override
    public List<String> getLastNames(final List<Student> students) {
        return extractField(students, Student::getLastName);
    }

    @Override
    public List<String> getGroups(final List<Student> students) {
        return extractField(students, Student::getGroup);
    }

    @Override
    public List<String> getFullNames(final List<Student> students) {
        return extractField(students, StudentDB::getFullName);
    }

    @Override
    public Set<String> getDistinctFirstNames(final List<Student> students) {
        return students.stream()
                .map(Student::getFirstName)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public String getMinStudentFirstName(final List<Student> students) {
        return students.stream()
                .min(Comparator.naturalOrder())
                .map(Student::getFirstName)
                .orElse("");
    }

    @Override
    public List<Student> sortStudentsById(final Collection<Student> students) {
        return sorted(students.stream(), Comparator.comparingInt(Student::getId));
    }

    @Override
    public List<Student> sortStudentsByName(final Collection<Student> students) {
        return sortedByName(students.stream());
    }

    @Override
    public List<Student> findStudentsByFirstName(final Collection<Student> students, final String name) {
        return sortedByName(extractWithField(students, Student::getFirstName, name));
    }

    @Override
    public List<Student> findStudentsByLastName(final Collection<Student> students, final String name) {
        return sortedByName(extractWithField(students, Student::getLastName, name));
    }

    @Override
    public List<Student> findStudentsByGroup(final Collection<Student> students, final String group) {
        return sortedByName(extractWithField(students, Student::getGroup, group));
    }

    @Override
    public Map<String, String> findStudentNamesByGroup(final Collection<Student> students, final String group) {
        return extractWithField(students, Student::getGroup, group)
                .collect(Collectors.toMap(
                        Student::getLastName,
                        Student::getFirstName,
                        BinaryOperator.minBy(String::compareTo)
                ));
    }

    @Override
    public List<Group> getGroupsByName(Collection<Student> students) {
        return getGroupsBy(students, COMPARING_NAME);
    }

    @Override
    public List<Group> getGroupsById(Collection<Student> students) {
        return getGroupsBy(students, Comparator.naturalOrder());
    }

    @Override
    public String getLargestGroup(Collection<Student> students) {
        return maxGroup(groupByGroup(students.stream()));
    }

    @Override
    public String getLargestGroupFirstName(Collection<Student> students) {
        return maxGroup(groupByGroup(students.stream(), Collectors.mapping(Student::getFirstName, Collectors.toSet())));
    }

    @Override
    public String getMostPopularName(Collection<Student> students) {
        return maxGroup(
                groupBy(students.stream(), StudentDB::getFullName,
                        Collectors.mapping(Student::getGroup, Collectors.toSet())), false);
    }

    private static String getFullName(final Student student) {
        return student.getFirstName() + " " + student.getLastName();
    }

    private static <E> List<E> extractField(final List<Student> students, final Function<Student, E> fieldGetter) {
        return students.stream().map(fieldGetter).collect(Collectors.toList());
    }

    private static List<Student> sortedByName(final Stream<Student> stream) {
        return sorted(stream, COMPARING_NAME);
    }

    private static <E> List<E> sorted(final Stream<E> stream, final Comparator<E> cmp) {
        return stream.sorted(cmp).collect(Collectors.toList());
    }

    private static Stream<Student> extractWithField(final Collection<Student> students,
                                                    final Function<Student, String> fieldGetter,
                                                    final String fieldValue) {
        return students.stream().filter(s -> fieldValue.equals(fieldGetter.apply(s)));
    }

    private static List<Group> getGroupsBy(Collection<Student> students, Comparator<Student> cmp) {
        return sorted(groupByGroup(students.stream().sorted(cmp)).map(kv -> new Group(kv.getKey(), kv.getValue())),
                Comparator.comparing(Group::getName));
    }

    private static Stream<Entry<String, List<Student>>> groupByGroup(final Stream<Student> stream) {
        return groupByGroup(stream, Collectors.toList());
    }

    private static <V> Stream<Entry<String, V>> groupByGroup(final Stream<Student> stream,
                                                             final Collector<Student, ?, V> valueCollector) {
        return groupBy(stream, Student::getGroup, valueCollector);
    }

    private static <K, V> Stream<Entry<K, V>> groupBy(final Stream<Student> stream,
                                                      final Function<Student, K> keyGetter,
                                                      final Collector<Student, ?, V> valueCollector) {
        return stream.collect(Collectors.groupingBy(keyGetter, valueCollector)).entrySet().stream();
    }

    private static <C extends Collection> String maxGroup(final Stream<Entry<String, C>> stream) {
        return maxGroup(stream, true);
    }

    private static <C extends Collection> String maxGroup(final Stream<Entry<String, C>> stream, boolean pickSmallestName) {
        return stream
                .max(Comparator
                        .comparingInt((Entry<String, C> kv) -> kv.getValue().size())
                        .thenComparing(Entry::getKey, pickSmallestName ? Comparator.<String>reverseOrder() : Comparator.<String>naturalOrder()))
                .map(Entry::getKey)
                .orElse("");
    }
}
