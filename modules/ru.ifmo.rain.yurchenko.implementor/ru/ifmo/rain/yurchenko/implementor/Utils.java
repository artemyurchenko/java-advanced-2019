package ru.ifmo.rain.yurchenko.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class
 */
final class Utils {
    /**
     * Equivalence class for methods with the same signature. Meant to be used in a hashtable.
     */
    private static class MethodSignature {
        /**
         * Representative of the equivalence class
         */
        private final Method method;

        /**
         * Construct equivalence class of {@code method}
         *
         * @param method method to construct equivalence class of
         */
        MethodSignature(Method method) {
            this.method = method;
        }

        /**
         * Get underlying method
         *
         * @return representative of the equivalence class
         */
        Method getMethod() {
            return method;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean equals(Object obj) {
            if (obj instanceof MethodSignature) {
                var other = (MethodSignature) obj;
                return method.getName().equals(other.method.getName())
                        && method.getReturnType().equals(other.method.getReturnType())
                        && Arrays.equals(method.getParameterTypes(), other.method.getParameterTypes());
            }
            return false;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public int hashCode() {
            return method.getName().hashCode() ^ method.getReturnType().hashCode();
        }
    }

    /**
     * String to append at the end of mockup class name.
     */
    static final String NAME_APPENDIX = "Impl";

    /**
     * Return type name in the form of "package.name.OuterClass.InnerClass"
     *
     * @param type to get canonical name of
     * @return canonical name
     */
    static String canonicalize(Type type) {
        return type.getTypeName().replace('$', '.');
    }

    /**
     * Gather all abstract methods of {@code aClass}, including abstract methods
     * of all superclasses and annotation superclass if present.
     *
     * @param aClass class to get abstract methods of
     * @return a list of all abstract methods
     */
    static List<Method> getAllAbstractMethods(Class<?> aClass) {
        var set = new HashSet<MethodSignature>();
        for (var method : aClass.getMethods()) {
            if (Modifier.isAbstract(method.getModifiers())) {
                set.add(new MethodSignature(method));
            }
        }
        for (var c = aClass; c != null; c = c.getSuperclass()) {
            for (var method : c.getDeclaredMethods()) {
                if (Modifier.isAbstract(method.getModifiers())) {
                    set.add(new MethodSignature(method));
                }
            }
        }
        return set.stream().map(MethodSignature::getMethod).collect(Collectors.toList());
    }

    /**
     * Create all directories on the path to {@code outputPath}
     *
     * @param outputPath path to create directories to
     * @throws ImplerException if I/O error occurs
     */
    static void createParents(Path outputPath) throws ImplerException {
        if (outputPath.getParent() != null) {
            try {
                Files.createDirectories(outputPath.getParent());
            } catch (IOException e) {
                throw new ImplerException("Failure to create path " + outputPath, e);
            }
        }
    }

    /**
     * Remove {@code path}.
     * <p>
     * If {@code path} is a directory then remove all of its contents as well.
     *
     * @param path path to remove.
     * @throws IOException if I/O error occurs
     */
    static void remove(Path path) throws IOException {
        if (Files.isDirectory(path)) {
            try (var dirStream = Files.newDirectoryStream(path)) {
                for (var entry : dirStream) {
                    remove(entry);
                }
            }
            Files.delete(path);
        } else {
            Files.deleteIfExists(path);
        }
    }
}
