package ru.ifmo.rain.yurchenko.implementor;

import java.lang.reflect.Constructor;

import static ru.ifmo.rain.yurchenko.implementor.Utils.NAME_APPENDIX;

/**
 * Specialization of {@link ExecutableBuilder} to build {@link Constructor}.
 *
 * @param <T> the class in which constructor is declared
 */
public class ConstructorBuilder<T> extends ExecutableBuilder<Constructor<T>> {
    /**
     * Construct builder of the specified constructor.
     *
     * @param constructor constructor to build, must be not final
     */
    ConstructorBuilder(Constructor<T> constructor) {
        super(constructor);
    }

    /**
     * {@inheritDoc}
     *
     * Add call to the constructor of the super-class.
     */
    @Override
    protected void fillBody() {
        body.append("super");
        body.append("(");
        for (var parameter : executable.getParameters()) {
            body.append(parameter.getName()).append(", ");
        }
        if (executable.getParameterCount() > 0) {
            body.setLength(body.length() - 2);
        }
        body.append(");");
    }

    /**
     * Does nothing because constructors don't have return type.
     */
    @Override
    protected void applyReturnType() {
    }

    /**
     * Build constructor name (same as the name of the declaring class).
     */
    @Override
    protected void applyName() {
        signature.append(executable.getDeclaringClass().getSimpleName()).append(NAME_APPENDIX);
    }
}
