package ru.ifmo.rain.yurchenko.implementor;

import info.kgeorgiy.java.advanced.implementor.ImplerException;
import org.jetbrains.annotations.NotNull;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Objects;

/**
 * Provider of a command-line interface
 */
public class Main {
    /**
     * Usage message constant.
     * <p>
     * Value: {@value #USAGE}
     */
    private static final String USAGE = "USAGE: [jar] CLASS_TOKEN PATH";
    /**
     * Jar construction option.
     * <p>
     * Value: {@value #JAR_SWITCH}
     */
    private static final String JAR_SWITCH = "jar";

    /**
     * Command-line interface to implement class and generate jar.
     *
     * @param args must conform to {@value #USAGE}.
     */
    public static void main(@NotNull String[] args) {
        if (args.length > 0 && JAR_SWITCH.equals(args[0])) {
            jarMain(args);
        } else {
            classMain(args);
        }
    }

    /**
     * Command-line interface to build class implementation java file.
     *
     * @param args must be in the form of "CLASS_TOKEN PATH".
     */
    private static void classMain(@NotNull String[] args) {
        if (args.length != 2) {
            System.err.println(USAGE);
            return;
        }
        var tokenValidator = new TokenValidator(args[0]);
        if (!tokenValidator.isValid()) {
            System.err.println(tokenValidator.getMessage());
            return;
        }
        Class<?> token = Objects.requireNonNull(tokenValidator.getValue());
        var pathValidator = new PathValidator(args[1]);
        if (!pathValidator.isValid()) {
            System.err.println(pathValidator.getMessage());
            return;
        }
        Path path = Objects.requireNonNull(pathValidator.getValue());
        try {
            new JarImplementor().implement(token, path);
        } catch (ImplerException e) {
            System.err.println("Couldn't generate implementation");
        }
    }

    /**
     * Command-line interface to build jar with class implementation.
     *
     * @param args must be in the form of "jar CLASS_TOKEN JAR_FILE_PATH".
     */
    private static void jarMain(@NotNull String[] args) {
        if (args.length != 3) {
            System.err.println(USAGE);
            return;
        }
        if (!JAR_SWITCH.equals(args[0])) {
            System.err.println(USAGE);
            return;
        }
        var tokenValidator = new TokenValidator(args[1]);
        if (!tokenValidator.isValid()) {
            System.err.println(tokenValidator.getMessage());
            return;
        }
        Class<?> token = Objects.requireNonNull(tokenValidator.getValue());
        var jarPathValidator = new JarPathValidator(args[2]);
        if (!jarPathValidator.isValid()) {
            System.err.println(jarPathValidator.getMessage());
            return;
        }
        Path jarPath = Objects.requireNonNull(jarPathValidator.getValue());
        try {
            new JarImplementor().implementJar(token, jarPath);
        } catch (ImplerException e) {
            System.err.println("Couldn't generate jar");
        }
    }

    /**
     * A class for {@link Class} tokens validation.
     */
    private static class TokenValidator extends AbstractValidator<Class<?>> {
        /**
         * Find valid class by {@code tokenName}.
         * <p>
         * In case class is not found, appropriate message is set.
         *
         * @param tokenName a token string representing {@code Class}.
         */
        TokenValidator(final @NotNull String tokenName) {
            try {
                value = Class.forName(tokenName);
            } catch (ClassNotFoundException e) {
                message = String.format("Class '%s' not found", tokenName);
            }
        }
    }

    /**
     * A class for {@link Path} validation.
     */
    private static class PathValidator extends AbstractValidator<Path> {
        /**
         * Get valid path from {@code pathName}.
         * <p>
         * In case path is invalid, appropriate message is set.
         *
         * @param pathName a string representing path
         */
        PathValidator(final @NotNull String pathName) {
            try {
                value = Path.of(pathName);
            } catch (InvalidPathException e) {
                message = String.format("Invalid path '%s'", pathName);
            }
        }
    }

    /**
     * A class for jar file paths validation.
     * <p>
     * A jar file path is considered invalid if it isn't a correct path or if it doesn't end with ".jar".
     */
    private static class JarPathValidator extends AbstractValidator<Path> {
        /**
         * Get valid path form {@code jarPathName}.
         * <p>
         * In case {@code jarPathName} is not a valid jar path according to the description of {@link JarPathValidator},
         * appropriate message is set.
         *
         * @param jarPathName a string representing jar path
         */
        JarPathValidator(final @NotNull String jarPathName) {
            try {
                if (!jarPathName.endsWith(".jar")) {
                    message = String.format("'%s' extension is not '.jar'", jarPathName);
                    return;
                }
                value = Path.of(jarPathName);
            } catch (InvalidPathException e) {
                message = String.format("Invalid path '%s'", jarPathName);
            }
        }
    }
}

