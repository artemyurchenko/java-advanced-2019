package ru.ifmo.rain.yurchenko.implementor;

import org.jetbrains.annotations.Nullable;

/**
 * Basic implementation of {@link Validator}.
 *
 * Provides {@code message} and {@code value} fields to ancestors.
 * @param <E> type to validate
 */
public abstract class AbstractValidator<E> implements Validator<E> {
    /**
     * Description of the construction problem
     */
    @Nullable
    protected String message = null;
    /**
     * Constructed valid value
     */
    @Nullable
    protected E value = null;

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public E getValue() {
        return value;
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessage() {
        return message;
    }
}
