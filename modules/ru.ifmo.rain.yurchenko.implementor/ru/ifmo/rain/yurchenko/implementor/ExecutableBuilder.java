package ru.ifmo.rain.yurchenko.implementor;

import java.lang.reflect.Executable;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;

/**
 * A common functionality class to build Executables.
 *
 * @param <E> {@link Executable} to build.
 */
public abstract class ExecutableBuilder<E extends Executable> {
    /**
     * {@link Executable} to build.
     */
    final E executable;
    /**
     * Signature of the executable is built here.
     */
    final StringBuilder signature = new StringBuilder();
    /**
     * Body of the executable is built here.
     */
    final StringBuilder body = new StringBuilder();

    /**
     * Construct builder of the specified executable.
     *
     * @param executable executable to build, must be not final
     */
    ExecutableBuilder(E executable) {
        assert !Modifier.isFinal(executable.getModifiers());
        this.executable = executable;
    }

    /**
     * Build signature of the executable
     *
     * @return built signature
     */
    String buildSignature() {
        if (signature.length() > 0) {
            return signature.toString();
        }
        applyModifiers();
        applyGenericParameters();
        applyReturnType();
        applyName();
        applyParameters();
        applyExceptions();
        return signature.toString();
    }

    /**
     * Build body of the executable if it hasn't been built yet
     *
     * @return built body
     */
    String buildBodyStatement() {
        if (body.length() > 0) {
            return body.toString();
        }
        fillBody();
        return body.toString();
    }

    /**
     * Add all necessary actions into the body builder.
     */
    protected abstract void fillBody();

    /**
     * Build executable modifiers.
     */
    private void applyModifiers() {
        int mod = executable.getModifiers();
        String visibility;
        if (Modifier.isPublic(mod)) {
            visibility = "public";
        } else if (Modifier.isProtected(mod)) {
            visibility = "protected";
        } else if (Modifier.isPrivate(mod)) {
            visibility = "private";
        } else {
            visibility = "";
        }
        if (!visibility.isEmpty()) {
            signature.append(visibility).append(" ");
        }
    }

    /**
     * Build executable generic parameters.
     */
    protected void applyGenericParameters() {
        var typeParameters = executable.getTypeParameters();
        if (typeParameters.length == 0) {
            return;
        }
        signature.append("<");
        for (var type : typeParameters) {
            signature.append(type.getName());
            var bounds = type.getBounds();
            if (bounds.length != 0) {
                signature.append(" extends ");
                for (var bound : bounds) {
                    signature.append(bound.getTypeName()).append(" & ");
                }
                signature.setLength(signature.length() - 3);
            }
            signature.append(", ");
        }
        signature.setLength(signature.length() - 2);
        signature.append("> ");
    }

    /**
     * Build executable return type.
     */
    protected abstract void applyReturnType();

    /**
     * Build executable name.
     */
    protected abstract void applyName();

    /**
     * Build executable parameters.
     */
    private void applyParameters() {
        signature.append("(");
        for (var parameter : executable.getParameters()) {
            applyParameter(parameter);
            signature.append(", ");
        }
        if (executable.getParameterCount() > 0) {
            signature.setLength(signature.length() - 2);
        }
        signature.append(") ");
    }

    /**
     * Build specified paramter of the executable.
     *
     * @param parameter parameter to build.
     */
    private void applyParameter(Parameter parameter) {
        var type = Utils.canonicalize(parameter.getParameterizedType());
        signature.append(type).append(" ").append(parameter.getName());
    }

    /**
     * Build executable exceptions.
     */
    private void applyExceptions() {
        var exceptionTypes = executable.getGenericExceptionTypes();
        if (exceptionTypes.length == 0) {
            return;
        }
        signature.append("throws ");
        for (var exc : exceptionTypes) {
            applyException(exc);
        }
        signature.setLength(signature.length() - 2);
    }

    /**
     * Build specified exception
     *
     * @param exception exception to build
     */
    private void applyException(Type exception) {
        signature.append(Utils.canonicalize(exception)).append(", ");
    }
}

